
var tgb = {
	numbers   : ['no more','one','two','three','four','five','six','seven','eight','nine','ten','eleven','twelve','thirteen','fourteen','fifteen','sixteen','seventeen','eighteen','nineteen','twenty'],
	color     : 'green',
	number    : 10,
	objects   : 'bottles',
	adjective : 'hanging',
	
	generateLyrics : function() {
		
		let lyrics = '';
		
		for (var i = this.number; i>0; i--) {
			lyrics += '<p style="color:' + this.color + '">' + this.numbers[i] + ' ' + this.color + ' ' + (i === 1 ? this.makeSingular(this.objects) : this.objects) + ' ' + this.adjective + ' on the wall, ';
			lyrics += 'And if one ' + this.color + ' ' + this.makeSingular(this.objects) + ' should accidentally fall, there&rsquo;ll be ' + this.numbers[i-1] + ' ';
			lyrics += this.color + ' ' +  (i === 2 ? this.makeSingular(this.objects) : this.objects) + ' ' + this.adjective + ' on the wall.</p>';
		}
		$('#lyrics').html(lyrics);
		
	},
	
	makeSingular : function(noun) {
		if (noun.substr(-3) === 'ies') {
			return noun.substr(0, noun.length - 3) + 'y';
		} else {
			return noun.substr(0, noun.length - 1);
		}
	},
	setColor : function(color) {
		this.color = color;
		this.generateLyrics();
	},
	setObjects : function(objects) {
		this.objects = objects;
		this.generateLyrics();
	},
	setNumber : function(number) {
		this.number = number;
		this.generateLyrics();
	},
	setAdjective : function(adjective) {
		this.adjective = adjective;
		this.generateLyrics();
	}
}

$(document).ready(function() {
	
	tgb.generateLyrics();
	
});
