<?php

require_once 'src/tgb.php';

function generateLyrics()
{
	$app        = new Tgb();
	$objects    = !empty($_REQUEST['number']) ? intval($_REQUEST['number']) : 10;
	$objectName = !empty($_REQUEST['objectName']) ? $_REQUEST['objectName'] : 'bottles';
	
	for($i = $objects; $i > 0; $i--) {
		$app-> setObjects( $i, $_REQUEST['color'] ?? 'green' , $objectName) ;
		echo nl2br($app->generateLyrics() );
	}	
}

